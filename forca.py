import random

def titulo():
    print('********************************')
    print('******* Jogo da Forca **********')
    print('********************************')

def carregar_frase():
    # arquivo = open('arquivo.txt', 'r')
    busca_arquivo = []

    with open('arquivo.txt') as arquivo:
        for linha in arquivo:
            busca_arquivo.append(linha.lower().strip())

    palavra_randomica = random.randrange(0, len(busca_arquivo))
    return busca_arquivo[palavra_randomica]

def caracteres(palavra):
    return ['_' for letra in palavra]

def perdeu(palavra_secreta):
    print("Puxa, você foi enforcado!")
    print("A palavra era {}".format(palavra_secreta))
    print("    _______________         ")
    print("   /               \       ")
    print("  /                 \      ")
    print("//                   \/\  ")
    print("\|   XXXX     XXXX   | /   ")
    print(" |   XXXX     XXXX   |/     ")
    print(" |   XXX       XXX   |      ")
    print(" |                   |      ")
    print(" \__      XXX      __/     ")
    print("   |\     XXX     /|       ")
    print("   | |           | |        ")
    print("   | I I I I I I I |        ")
    print("   |  I I I I I I  |        ")
    print("   \_             _/       ")
    print("     \_         _/         ")
    print("       \_______/           ")

def ganhou():
    print("Parabéns, você ganhou!")
    print("       ___________      ")
    print("      '._==_==_=_.'     ")
    print("      .-\\:      /-.    ")
    print("     | (|:.     |) |    ")
    print("      '-|:.     |-'     ")
    print("        \\::.    /      ")
    print("         '::. .'        ")
    print("           ) (          ")
    print("         _.' '._        ")
    print("        '-------'       ")

def progresso_game(erros):
    print("  _______     ")
    print(" |/      |    ")

    if (erros == 1):
        print(" |      (_)   ")
        print(" |            ")
        print(" |            ")
        print(" |            ")

    if (erros == 2):
        print(" |      (_)   ")
        print(" |      \     ")
        print(" |            ")
        print(" |            ")

    if (erros == 3):
        print(" |      (_)   ")
        print(" |      \|    ")
        print(" |            ")
        print(" |            ")

    if (erros == 4):
        print(" |      (_)   ")
        print(" |      \|/   ")
        print(" |            ")
        print(" |            ")

    if (erros == 5):
        print(" |      (_)   ")
        print(" |      \|/   ")
        print(" |       |    ")
        print(" |            ")

    if (erros == 6):
        print(" |      (_)   ")
        print(" |      \|/   ")
        print(" |       |    ")
        print(" |      /     ")

    if (erros == 7):
        print(" |      (_)   ")
        print(" |      \|/   ")
        print(" |       |    ")
        print(" |      / \   ")

    print(" |            ")
    print("_|___         ")
    print()

def forca():
        titulo()
        palavra = carregar_frase()
        representacao = caracteres(palavra)

        acerto = False
        erros = False
        erro_chute = 0
        tentativas = 7

        while not acerto and not erros:

            chute = input('Digite um chute: ').lower()

            if chute in palavra:
                index = 0
                for letra in palavra:
                    if chute == letra:
                        representacao[index] = chute
                        acerto = '_' not in representacao
                    index += 1
            else:
                erro_chute += 1
                tentativas -= 1
                print('Você errou! restam {} tentativas'.format(tentativas))
                progresso_game(erro_chute)
            erros = erro_chute == 7
            print(representacao)

        if erros:
            perdeu(palavra)
        else:
            ganhou()

if __name__ == "__main__":
    forca()

