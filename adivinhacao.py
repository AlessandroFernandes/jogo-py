import random

def Adivinhacao():
    print('****************************')
    print('******** Teste Game ********')
    print('****************************\n')
    print('Adivinhe o número :)\n')

    numero_certo = random.randint(1, 9)
    numero_tentativas = 0
    pontos = 100

    level = int(input('Digite o level que você quer, 1(fácil) / 2(normal)/ 3(difícil): '))
    if(level == 1):
        numero_tentativas = 20
    elif(level == 2):
        numero_tentativas = 15
    elif(level == 3):
        numero_tentativas = 10
    else:
        print("Numero inválido!")
        numero_tentativas = 0

    for tentativa in range(1, numero_tentativas +1):
        print('Jogada {} de {}'.format(tentativa, numero_tentativas))
        chute = input('Digite numero de {} à {}: '.format(0, 10))
        print(f'Você digitou: {chute}')

        chuteInt = int(chute)

        if(chuteInt < 0 or chuteInt > 10):
            print("Você precisa digita um numero entre 0 e 10")
            continue

        acertou = chuteInt == numero_certo
        maior = chuteInt >= numero_certo

        if(acertou):
            print("Você ganhou! Total de pontos{}".format(pontos))
            break
        elif(maior):
            erros = abs(numero_certo - chuteInt)
            pontos-= erros*2
            print(f"Errou. Você digitou um numero maior {chuteInt}")
        else:
            erros = abs(numero_certo - chuteInt)
            pontos -= erros*3
            print(f"Errou. Você digitou um numero menor {chuteInt}")

if(__name__ == "__main__"):
    Adivinhacao()